@extends('layouts.master')

@push('headers')
{{-- @FilemanagerScript --}}
 <script src="https://cdn.tiny.cloud/1/q23ykeb8xa9nh7olkcm2xlk8fvpzqfdi1u9k9oe7u03yjxpx/tinymce/4/tinymce.min.js" referrerpolicy="origin"></script>
@endpush

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Pertanyaan Baru</h4>
        <p class="card-description">
            Silakan tanyakan apapun yang kalian ingin tanyakan
        </p>
<form action="/pertanyaan" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="title">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul Pertanyaan">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="title">Content</label>
        <textarea type="text" class="form-control" name="content" id="content" placeholder="Masukkan pertanyaan anda"></textarea>
        @error('content')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="title">Gambar</label>
        <input type="file" class="form-control" name="gambar" id="" placeholder="Silakan pilih salah satu gambar">
    </div>
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    <div class="form-group">
        <label for="title">Kategori</label>
        <select class="form-control" name="kategori_id" id="" placeholder="Masukkan Kategori">
            <option value="">--Pilih Salah satu Kategoti--</option>
            @forelse ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama_kategori}}</option>    
            @empty
                <option value="">Tidak ada Kategori</option>
            @endforelse
        @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/pertanyaan" class="btn btn-light"> Kembali </a>
</form>
      </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
        var editor_config = {
        path_absolute : "/",
        selector: "textarea",
        height : "480",
        plugins: [
        "advlist autolink autosave lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile restoredraft undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
        });
        }
    };

    tinymce.init(editor_config);
</script>
@endpush