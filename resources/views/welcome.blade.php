<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>Forum</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <!-- themefy-icon -->
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <!-- slick slider -->
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <!-- venobox popup -->
  <link rel="stylesheet" href="plugins/Venobox/venobox.css">
  <!-- aos -->
  <link rel="stylesheet" href="plugins/aos/aos.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">
  
  <!--Favicon-->
  <link rel="shortcut icon" href="{{asset('template/images/Icon_forum.png')}}">
</head>

<body>
  

<!-- navigation -->
<section class="fixed-top navigation">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
      <a class="navbar-brand" href="/pertanyaan"><img src="{{asset('template/images/Logo_forum.png')}}" width="120px" alt="logo"></a>
      <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- navbar -->
      <div class="collapse navbar-collapse text-center" id="navbar">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link page-scroll" href="/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="#feature">Feature</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="#team">Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="pertanyaan">Forum</a>
          </li>
        </ul>
            @auth
            <a class="btn btn-primary ml-lg-3 primary-shadow" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="ti-power-off text-primary"></i>
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @endauth
            @guest
            <a href="/login" class="btn btn-primary ml-lg-3 primary-shadow">Login</a>
            @endguest
      </div>
    </nav>
  </div>
</section>
<!-- /navigation -->

<!-- hero area -->
<section class="hero-section hero" data-background="" style="background-image: url(images/hero-area/banner-bg.png);">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center zindex-1">
        <h1 class="mb-3">Forum<br>
          Programmer</h1>
        <p class="mb-4">Yuk bikin komunitas programming online yang nyaman.<br>
          Kamu boleh bertanya, boleh juga berbagi.</p>
        <a href="/pertanyaan" class="btn btn-secondary btn-lg">Forum</a>
        <!-- banner image -->
        <img class="img-fluid w-100 banner-image" src="images/hero-area/banner-img.png" alt="banner-img">
      </div>
    </div>
  </div>
  <!-- background shapes -->
  <div id="scene">
    <img class="img-fluid hero-bg-1 up-down-animation" src="images/background-shape/feature-bg-2.png" alt="">
    <img class="img-fluid hero-bg-2 left-right-animation" src="images/background-shape/seo-ball-1.png" alt="">
    <img class="img-fluid hero-bg-3 left-right-animation" src="images/background-shape/seo-half-cycle.png" alt="">
    <img class="img-fluid hero-bg-4 up-down-animation" src="images/background-shape/green-dot.png" alt="">
    <img class="img-fluid hero-bg-5 left-right-animation" src="images/background-shape/blue-half-cycle.png" alt="">
    <img class="img-fluid hero-bg-6 up-down-animation" src="images/background-shape/seo-ball-1.png" alt="">
    <img class="img-fluid hero-bg-7 left-right-animation" src="images/background-shape/yellow-triangle.png" alt="">
    <img class="img-fluid hero-bg-8 up-down-animation" src="images/background-shape/service-half-cycle.png" alt="">
    <img class="img-fluid hero-bg-9 up-down-animation" src="images/background-shape/team-bg-triangle.png" alt="">
  </div>
</section>
<!-- /hero-area -->

<!-- feature -->
<section class="section feature mb-0" id="feature">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-title">Awesome Features</h2>
        <p class="mb-100">Berikut ini Fitur yang ada di Forum Programmer</p>
      </div>
      <!-- feature item -->
      <div class="col-md-6 mb-80">
        <div class="d-flex feature-item">
          <div>
            <i class="ti-ruler-pencil feature-icon mr-4"></i>
          </div>
          <div>
            <h4>Easy Login / Register</h4>
            <p>Kalian bisa melakukan Login dan Register dengan mudah</p>
          </div>
        </div>
      </div>
      <!-- feature item -->
      <div class="col-md-6 mb-80">
        <div class="d-flex feature-item">
          <div>
            <i class="ti-layout-cta-left feature-icon mr-4"></i>
          </div>
          <div>
            <h4>Easy Customize</h4>
            <p>Kalian bisa membuat, mengedit dan menghapus pertanyaan
              dan Jawaban hanya dengan 1x Klik
            </p>
          </div>
        </div>
      </div>
      <!-- feature item -->
      <div class="col-md-6 mb-80">
        <div class="d-flex feature-item">
          <div>
            <i class="ti-split-v-alt feature-icon mr-4"></i>
          </div>
          <div>
            <h4>Bug free Code</h4>
            <p>Dapat menanyakan bug kepada programmer senior</p>
          </div>
        </div>
      </div>
      <!-- feature item -->
      <div class="col-md-6 mb-80">
        <div class="d-flex feature-item">
          <div>
            <i class="ti-layers-alt feature-icon mr-4"></i>
          </div>
          <div>
            <h4>Easy Komentar</h4>
            <p>Kalian bisa bantu menjawab dengan mudah jika ada Programmer yang Kesulitan.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <img class="feature-bg-1 up-down-animation" src="images/background-shape/feature-bg-1.png" alt="bg-shape">
  <img class="feature-bg-2 left-right-animation" src="images/background-shape/feature-bg-2.png" alt="bg-shape">
</section>
<!-- /feature -->

<!-- team -->
<section class="section-lg team" id="team">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-title">Our Team</h2>
        <p class="mb-100">Dibawah ini adalah para pembuat Forum Programmer<br>Salam Kenal Semuanya.</p>
      </div>
    </div>
    <div class="col-10 mx-auto">
      <div class="team-slider">
        <!-- team-member -->
        <div class="team-member">
          <div class="d-flex mb-4">
            <div class="mr-3">
              <img class="rounded-circle img-fluid" src="images/team/team-1.jpg" alt="team-member">
            </div>
            <div class="align-self-center">
              <h4>Muhamad Yoga Subarkah</h4>
              <h6 class="text-color">Web Designer</h6>
            </div>
          </div>
          <p>Perkenalkan saya adalah salah satu Karawan swasta asal Jakarta, yang ingin belajar IT Lebih dalam.</p>
        </div>
        <!-- team-member -->
        <div class="team-member">
          <div class="d-flex mb-4">
            <div class="mr-3">
              <img class="rounded-circle img-fluid" src="images/team/team-2.jpg" alt="team-member">
            </div>
            <div class="align-self-center">
              <h4>Ragil Andika Johan</h4>
              <h6 class="text-color">Web Developer</h6>
            </div>
          </div>
          <p>Perkenalkan saya Johan asal Aceh. Saya baru saja lulus dari Jurusan Teknologi Informasi.</p>
        </div>
        <!-- team-member -->
        <div class="team-member">
          <div class="d-flex mb-4">
            <div class="mr-3">
              <img class="rounded-circle img-fluid" src="images/team/team-1.jpg" alt="team-member">
            </div>
            <div class="align-self-center">
              <h4>Nurul Hidayat</h4>
              <h6 class="text-color">Programmer</h6>
            </div>
          </div>
          <p>Perkenalkan saya Hidayat asal Semarang. Saya merupakan karyawan swasta yang ingin belajar tentang IT.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- backgound image -->
  <img src="images/backgrounds/team-bg.png" alt="team-bg" class="img-fluid team-bg">
  <!-- background shapes -->
  <img class="team-bg-shape-1 up-down-animation" src="images/background-shape/seo-ball-1.png" alt="background-shape">
  <img class="team-bg-shape-2 left-right-animation" src="images/background-shape/seo-ball-1.png" alt="background-shape">
  <img class="team-bg-shape-3 left-right-animation" src="images/background-shape/team-bg-triangle.png" alt="background-shape">
  <img class="team-bg-shape-4 up-down-animation img-fluid" src="images/background-shape/team-bg-dots.png" alt="background-shape">
</section>
<!-- /team -->
<!-- footer -->
<footer class="footer-section footer" style="background-image: url(images/backgrounds/footer-bg.png);">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 text-center text-lg-left mb-4 mb-lg-0">
        <!-- logo -->
        <a href="/pertanyaan">
          <img class="img-fluid" src="template/images/Logo_forum.png" width="100px" alt="logo">
        </a>
      </div>
      <!-- footer menu -->
      <nav class="col-lg-8 align-self-center mb-5">
        <ul class="list-inline text-lg-right text-center footer-menu">
          <li class="list-inline-item active"><a href="/">Home</a></li>
          <li class="list-inline-item"><a class="page-scroll" href="#feature">Feature</a></li>
          <li class="list-inline-item"><a class="page-scroll" href="#team">Team</a></li>
          <li class="list-inline-item"><a class="page-scroll" href="/pertanyaan">Forum</a></li>
        </ul>
      </nav>
      <!-- footer social icon -->
      <nav class="col-12">
        <ul class="list-inline text-lg-right text-center social-icon">
          <li class="list-inline-item">
            <a class="facebook" href="#"><i class="ti-facebook"></i></a>
          </li>
          <li class="list-inline-item">
            <a class="twitter" href="#"><i class="ti-twitter-alt"></i></a>
          </li>
          <li class="list-inline-item">
            <a class="linkedin" href="#"><i class="ti-linkedin"></i></a>
          </li>
          <li class="list-inline-item">
            <a class="black" href="#"><i class="ti-github"></i></a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</footer>
<!-- /footer -->

<!-- jQuery -->
<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<!-- slick slider -->
<script src="plugins/slick/slick.min.js"></script>
<!-- venobox -->
<script src="plugins/Venobox/venobox.min.js"></script>
<!-- aos -->
<script src="plugins/aos/aos.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>

</body>
</html>