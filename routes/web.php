<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/pertanyaan', [App\Http\Controllers\PertanyaanController::class, 'index'])->name('pertanyaan');

Route::group(['middleware' => ['auth']], function () {
   
   Route::resource('kategori', KategoriController::class);
   Route::resource('profile', ProfileController::class);
    
});

Route::resource('pertanyaan', pertanyaanController::class);
Route::resource('jawaban', JawabanController::class);

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });