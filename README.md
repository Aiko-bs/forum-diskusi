## Final Project

## Anggota Kelompok

-   Wulandari Silalahi (@Aiko-bs)
-   Darusman Damanik (@midlaner)
-   Yofvi Ramadanni.B (@yovfiramdanni)
-   Irawan Oktavianus Zega (@irawanzega)
-   Al Azhar Ramadan (@AlAzharr)

## Tema Project

Forum Tanya Jawab

## Deskripsi Sistem

Dibuat menggunakan laravel 8, Aplikasi forum tanya jawab ini terdiri dari menu

-   halaman depan
-   crud kategori
-   crud pertanyaan
-   crud jawaban
-   edit profile

Dimana user harus membuat akun terlebih dahulu untuk memulai diskusi atau tanya jawab dengan user lain. Menambahkan kategori baru kemudian memulai pertanyaan user juga dapat menginput gambar pada pertanyaan ataupun jawaban.

## ERD

<p align="center"><img src="public/images/ERD_Forum.png"></p>

## Link Video

-   Link Google Drive Video Aplikasi : [https://drive.google.com/drive/folders/1lvxZ6Yd8p-oeT200FFxl1omocTYHRWRW?usp=share_link](https://drive.google.com/drive/folders/1lvxZ6Yd8p-oeT200FFxl1omocTYHRWRW?usp=share_link).
-   Link Deploy : [http://forum.ezscode.com/](http://forum.ezscode.com/).
